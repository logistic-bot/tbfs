//! Storage of files and metadata on disk

use crate::error::TbfsResult;
use crate::file::Metadata;
use crate::filetypes::name::ShortName;
use crate::filetypes::simple_tag::SimpleTag;
use crate::prelude::*;

// just `sled` is ambiguous with the library of the same name
#[allow(clippy::module_name_repetitions)]
pub mod sled_storage;
#[allow(clippy::module_name_repetitions)]
pub use sled_storage::SledStorage;

/// The file was inserted successfully
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum InsertSuccess {
    /// The file already existed, no changes were made.
    ExistingFile,
    /// You created a new file.
    NewFile,
}

/// A way to store [`crate::File`]s on disk.
pub trait Storage {
    // -------- Basic Functionality --------
    /// Insert a file into the storage, saving it.
    /// # Errors
    /// If the insert fails, this returns an error. This is
    /// implementation-defined.
    fn insert(&mut self, file: &File) -> TbfsResult<InsertSuccess>;
    /// Delete a file from the storage, returning the file
    /// # Errors
    /// May fail if the underlying database gives an error, or if the file doesn't exist
    fn delete(&mut self, hash: &Hash) -> TbfsResult<File>;
    /// Get a file from the storage by its hash.
    /// # Errors
    /// When the decoding of the filetype fails, or for implementation-defined
    /// reasons, this will return an error.
    fn get(&self, hash: &Hash) -> TbfsResult<File>;
    /// Get all hashes and filetypes.
    ///
    /// The order is not guaranteed to be consistent.
    /// # Errors
    /// May return an error if fetching or decoding the info from the database fails.
    ///
    /// Returns only the first error encountered.
    fn list(&self) -> TbfsResult<Vec<(Hash, Filetype)>>;

    // -------- Metadata --------
    /// Get a [`Metadata`] from the storage by its hash.
    /// # Errors
    /// When the decoding of the filetype fails, or for implementation-defined
    /// reasons, this will return an error.
    fn get_metadata(&self, hash: &Hash) -> TbfsResult<Metadata>;
    /// Get size of that file in bytes.
    /// # Errors
    /// May error if datastore fetch fails, or if the file doesn't exist.
    fn get_size(&self, hash: &Hash) -> TbfsResult<usize>;
    /// Get filetype of that file.
    /// # Errors
    /// May error if datastore fetch fails, or if the file doesn't exist, or if the filetype decoding fails.
    fn get_filetype(&self, hash: &Hash) -> TbfsResult<Filetype>;

    // -------- Find info relating to multiple files --------
    /// Get a canonical hash from a partial hash.
    ///
    /// For example, the canonical hash of `d83a9581f7aa8c704` is
    /// `d83a9581f7aa8c7040e882ad351d44348fc9bdac6a8719648adec83207045593`, as
    /// long as there is no other file hash that also starts with
    /// `d83a9581f7aa8c704`.
    ///
    /// This is used to allow humans to only remember the start of a hash.
    ///
    /// Returns None if no corresponding hash is found.
    /// # Errors
    /// May return an internal database error.
    ///
    /// Will return [`crate::error::Error::AmbiguousPartialHash`] if multiple
    /// matching hashes are found.
    ///
    /// Will return [`crate::error::Error::InvalidHashString`] if the given hash
    /// string is invalid.
    fn get_canonical_hash(&self, hash: &str) -> TbfsResult<Option<Hash>>;

    // -------- Bulk Get --------
    /// Get all hashes.
    ///
    /// The order is not guaranteed to be consistent.
    /// # Errors
    /// May return an error if fetching or decoding the info from the database fails.
    ///
    /// Returns only the first error encountered.
    fn get_all_hashes(&self) -> TbfsResult<Vec<Hash>>;
    /// Get all [`Metadata`]s from the database.
    ///
    /// The order is not guaranteed to be consistent.
    /// # Errors
    /// May return an error if fetching or decoding the info from the database fails.
    ///
    /// Returns only the first error encountered.
    fn get_all_metadatas(&self) -> TbfsResult<Vec<Metadata>>;
    /// Get all files with matching filetype
    /// # Errors
    /// May error if something goes wrong in the database, or with the decoding
    /// of the filetype.
    fn get_files_of_type(&self, filetype: &Filetype) -> TbfsResult<Vec<File>>;

    // -------- Find info about a file that is contained in one or more other files --------
    /// Get hashes of all simple tags pointing towards the given file
    /// # Errors
    /// May error if something goes wrong in the database, or with the decoding
    /// of simple tags.
    fn find_simple_tags(&self, hash: &Hash) -> TbfsResult<Vec<(Hash, SimpleTag)>>;
    /// Get all short names pointing towards the given file
    /// # Errors
    /// May error if something goes wrong in the database, or with the decoding
    /// of short names.
    fn find_short_names(&self, hash: &Hash) -> TbfsResult<Vec<(Hash, ShortName)>>;

    // -------- Backend Metadata --------
    /// Get total size on disk used by all the data and metadata
    /// # Errors
    /// May error if something goes wrong in the database.
    fn total_size_on_disk(&self) -> TbfsResult<u64>;
}
