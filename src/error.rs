//! Error types for TBFS

use std::string::FromUtf8Error;

use thiserror::Error;

use crate::prelude::*;

/// Result type for TBFS
pub type TbfsResult<T> = Result<T, Error>;

/// Error type for TBFS.
///
/// This should only include errors that happen at the storage layer, not
/// application errors.
#[derive(Error, Debug)]
pub enum Error {
    /// File with given hash does not exist
    #[error("File does not exist: {0:?}")]
    FileDoesNotExist(Hash),
    /// [`std::io::Error`]
    #[error("Internal input/output error: {0:?}")]
    InternalIO(#[from] std::io::Error),
    /// Error specific to a storage backend.
    ///
    /// This should *not* be used for nonexistent file or other such things.
    #[error("Internal storage backend error: {0:?}")]
    InternalStorageBackend(#[from] sled::Error),
    /// The saved filetype is not a valid utf8-encoded string.
    #[error("Corrupted filetype, expected utf8-encoded string: {0:?}")]
    CorruptedFiletype(FromUtf8Error),
    /// The given partial hash is ambiguous.
    #[error("Ambiguous partial hash {0:?}. Matching hashes: {1:?}")]
    AmbiguousPartialHash(Vec<u8>, Vec<Hash>),
    /// The given hash string is not a valid hash.
    #[error("Invalid hash string: {0}")]
    InvalidHashString(#[from] base16ct::Error),
    /// The given hash from the database is invalid
    #[error("Invalid hash from database: {0:?}")]
    InvalidHash(Vec<u8>),
    /// Expected a file of a specific type, but got another type
    #[error("Expected a file of type {expected}, but got {actual}")]
    InvalidFiletype {
        /// The expected filetype
        expected: Filetype,
        /// The filetype that was actually present
        actual: Filetype,
    },
    /// Failed to decode a bincode-encoded file
    #[error("Failed to decode bincode data: {0:?}")]
    BincodeDecode(#[from] bincode::error::DecodeError),
    /// Failed to encode data using bincode
    #[error("Failed to encode to bincode data: {0:?}")]
    BincodeEncode(#[from] bincode::error::EncodeError),
}
