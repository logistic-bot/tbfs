#![allow(clippy::unwrap_used)]

use crate::{
    filetypes::name::ShortName,
    prelude::{File, Filetype, Hash},
    storage::{SledStorage, Storage},
};

use color_eyre::Result;

/// Create an empty directory to store some test data.
/// Deletes all data in that directory before creation.
/// The directory will be somewhere like
/// `~/path/to/tbfs/source/code/tbfs_store/[name]`
/// # Panics
/// May panic. This is a test. Read the source code yourself.
pub fn test_path(name: &str) -> std::path::PathBuf {
    let path = std::path::Path::new("/tmp").join("tbfs_testing").join(name);
    let _ignore = std::fs::remove_dir_all(&path);
    std::fs::create_dir_all(&path).unwrap();
    path
}

/// Create a tmp file in a tmp directory with specified name.
/// # Panics
/// May panic. This is a test. Read the source code yourself.
pub fn test_file(test_name: &str, name: &str) -> (std::fs::File, std::path::PathBuf) {
    let path = std::path::Path::new("/tmp")
        .join("tbfs_store_testing")
        .join(test_name);
    std::fs::create_dir_all(&path).unwrap();
    let path = path.join(name);
    (std::fs::File::create(&path).unwrap(), path)
}

pub fn add_default_files(path: &std::path::PathBuf) -> Result<SledStorage, color_eyre::Report> {
    let mut store = SledStorage::new(path)?;
    let file1 = File::from_data(b"this is file1", Filetype::from("text::plain"));
    let file2 = File::from_data(
        b"this is file2, slightly bigger",
        Filetype::from("some::other::filetype"),
    );
    let file3 = File::from_data(
        b"this is file3, the biggest of them all",
        Filetype::from("text::plain"),
    );
    store.insert(&file1)?;
    store.insert(&file2)?;
    store.insert(&file3)?;
    Ok(store)
}

pub fn add_default_files_with_names(path: &std::path::PathBuf) -> Result<SledStorage> {
    let mut store = add_default_files(path)?;

    add_name(file1_hash()?, "File #1", &mut store)?;

    add_name(file2_hash()?, "File #2", &mut store)?;
    add_name(file2_hash()?, "Another name for file #2", &mut store)?;

    add_name(
        file3_hash()?,
        "Multi-line name for File #3\nLine #2 of the name of file #3\nThis is line #3",
        &mut store,
    )?;
    add_name(
        file3_hash()?,
        "Another multi-line name for File #3\nLine #2 of the name of file #3\nThis is line #3",
        &mut store,
    )?;

    Ok(store)
}

fn add_name(target_file: Hash, name: &str, store: &mut SledStorage) -> Result<()> {
    let name = ShortName::new(target_file, name.to_string());
    let name_file = File::try_from(name)?;
    store.insert(&name_file)?;
    Ok(())
}

pub fn file1_hash() -> Result<Hash, color_eyre::Report> {
    let hash = Hash::try_from(base16ct::lower::decode_vec(
        b"adc1f6e0aaa71ef83e60bff6c5e8e0606c127fb3234792a7ba416d88c4017517",
    )?)?;
    Ok(hash)
}

pub fn file2_hash() -> Result<Hash, color_eyre::Report> {
    let hash = Hash::try_from(base16ct::lower::decode_vec(
        b"7d5a6c6098220881d18ad7c76fbc9106b454409ecf03a2bf721074427c167daa",
    )?)?;
    Ok(hash)
}

pub fn file3_hash() -> Result<Hash, color_eyre::Report> {
    let hash = Hash::try_from(base16ct::lower::decode_vec(
        b"c3c22da74ea5e6f1228245e1a3c4eab01c74af76489f66f59180ba97f20cf834",
    )?)?;
    Ok(hash)
}
