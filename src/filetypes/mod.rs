//! Stuff relating to filetypes defined for tbfs.
//!
//! Complex tags, simple tags, tag cores, etc.
//!
//! Reference:
//! <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies#everything-is-a-file/>

pub mod name;
pub mod simple_tag;
