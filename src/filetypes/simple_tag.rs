//! Simple string tags
//!
//! These associate one string of text with one file.
//!
//! these should be used when it is unlikely that many files will have the same
//! tag, and the tag models a *string* and not a *concept*. when modeling a
//! *concept*, please use complex tags.
//!
//! Reference:
//! <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies#simple-string-tags/>

use crate::{error::Error, prelude::*};

use bincode::{Decode, Encode};
use colored::Colorize;

/// Filetype string representation for a simple tag
pub const FILETYPE_STR: &str = "tbfs::internal::tag::simple";

/// Filetype for a simple tag
pub fn filetype() -> Filetype {
    Filetype::new(vec![
        String::from("tbfs"),
        String::from("internal"),
        String::from("tag"),
        String::from("simple"),
    ])
}

/// Simple string tags
///
/// These associate one string of text with one file.
///
/// These should be used when it is unlikely that many files will have the same
/// tag, and the tag models a *string* and not a *concept*. When modeling a
/// *concept*, please use complex tags.
///
/// Reference:
/// <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies#simple-string-tags/>
#[derive(Clone, PartialEq, Eq, Debug, Encode, Decode)]
pub struct SimpleTag {
    /// The file to be tagged
    pub target_file: Hash,
    /// The tag to be applied
    pub content: String,
}

impl SimpleTag {
    /// Create a new [`SimpleTag`]. Does not check for existence of the target file
    pub const fn new(target_file: Hash, content: String) -> Self {
        Self {
            target_file,
            content,
        }
    }
}

impl TryFrom<SimpleTag> for File {
    type Error = Error;

    fn try_from(value: SimpleTag) -> Result<Self, Self::Error> {
        let data = bincode::encode_to_vec(value, bincode::config::standard())?;
        let file = Self::from_data(&data, FILETYPE_STR);
        Ok(file)
    }
}

impl TryFrom<File> for SimpleTag {
    type Error = Error;

    fn try_from(value: File) -> Result<Self, Self::Error> {
        let expected_filetype = filetype();
        if expected_filetype.eq(value.filetype()) {
            Ok(bincode::decode_from_slice(value.data(), bincode::config::standard())?.0)
        } else {
            Err(Error::InvalidFiletype {
                expected: expected_filetype,
                actual: value.filetype().clone(),
            })
        }
    }
}

impl std::fmt::Display for SimpleTag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Target: {}", self.target_file.to_string().yellow())?;
        write!(f, "Content: {}", self.content)
    }
}
