//! Giving names to files since 1904!
//!
//! These associate one string of text with one file, with the extra meaning
//! that that text is in some way the name of that file.
//!
//! These should *not* be used to have the full original filepath.
//!
//! Example usage: The movie "Arrival of a Train at La Ciotat" should have that
//! name.
//!
//! NOTE: In some cases, including this example, a complex indirect tag should
//! be used instead, with the name being used to name the tag core.
//!
//! Reference:
//! This is partially discussed in the complex tags section, altough this
//! implementation differs somewhat.
//! <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies#complex-indirect-tags/>

use crate::{error::Error, prelude::*};

use bincode::{Decode, Encode};
use colored::Colorize;

/// Filetype string representation for a short name
pub const FILETYPE_STR: &str = "tbfs::internal::name::short";

/// Filetype for a short name
pub fn filetype() -> Filetype {
    Filetype::new(vec![
        String::from("tbfs"),
        String::from("internal"),
        String::from("name"),
        String::from("short"),
    ])
}

/// Short names
///
/// These associate a name to a file.
///
/// See the module documentation for more information.
#[derive(Clone, PartialEq, Eq, Debug, Encode, Decode)]
#[cfg_attr(test, derive(serde::Serialize, serde::Deserialize))]
#[allow(clippy::module_name_repetitions)]
pub struct ShortName {
    /// The file to be named
    pub target_file: Hash,
    /// The name to be applied.
    ///
    /// Should be a single line of text.
    pub name: String,
}

impl ShortName {
    /// Create a new [`ShortName`]. Does not check for existence of the target file
    pub const fn new(target_file: Hash, name: String) -> Self {
        Self { target_file, name }
    }
}

impl TryFrom<ShortName> for File {
    type Error = Error;

    fn try_from(value: ShortName) -> Result<Self, Self::Error> {
        let data = bincode::encode_to_vec(value, bincode::config::standard())?;
        let file = Self::from_data(&data, FILETYPE_STR);
        Ok(file)
    }
}

impl TryFrom<File> for ShortName {
    type Error = Error;

    fn try_from(value: File) -> Result<Self, Self::Error> {
        let expected_filetype = filetype();
        if expected_filetype.eq(value.filetype()) {
            Ok(bincode::decode_from_slice(value.data(), bincode::config::standard())?.0)
        } else {
            Err(Error::InvalidFiletype {
                expected: expected_filetype,
                actual: value.filetype().clone(),
            })
        }
    }
}

impl std::fmt::Display for ShortName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Target: {}", self.target_file.to_string().yellow())?;
        write!(f, "Name: {}", self.name)
    }
}
