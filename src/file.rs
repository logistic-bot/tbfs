//! Files and related types

use std::string::FromUtf8Error;

use bincode::{Decode, Encode};
use colored::Colorize;
use generic_array::functional::FunctionalSequence;
use sha3::digest::generic_array::GenericArray;
use sled::IVec;

#[cfg(test)]
use insta::assert_yaml_snapshot;

/// Defines the structure of an individual file.
///
/// Relevant section:
/// <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies#data-schemas/>
///
/// This does not implement the proposed solutions(s), but only serves as an
/// identifier, essentially pushing the problem towards the client
/// implementations. This is good enough for now, since this is more of a
/// prototype than an actual implementation.
///
/// A filetype is represented with something along the lines of `image::png` or
/// `internal::tag::core`. Each part between the `::` is a [`FiletypeSegment`].
///
/// A central authority should be implemented to coordinate filetypes, and
/// prevent the same type of file to have multiple different filetypes
/// associated.
#[derive(Debug, Clone, Eq, PartialEq)]
#[cfg_attr(test, derive(serde::Serialize, serde::Deserialize))]
pub struct Filetype(Vec<String>);

impl Filetype {
    /// Create a new filetype from the given segments
    pub fn new(segments: Vec<String>) -> Self {
        Self(segments)
    }

    /// Return true if the associated file can be represented as plain text
    pub fn is_text(&self) -> bool {
        self.0.as_slice().first().map_or(false, |x| x == "text")
    }

    /// Return true if the associated file is an image
    pub fn is_image(&self) -> bool {
        self.0.as_slice().first().map_or(false, |x| x == "image")
    }

    /// Return true if the associated file is an internal tbfs filetype
    pub fn is_internal(&self) -> bool {
        self.0.as_slice().first().map_or(false, |x| x == "tbfs")
            && self
                .0
                .as_slice()
                .iter()
                .nth(1)
                .map_or(false, |x| x == "internal")
    }
}

#[cfg(test)]
mod test_filetype_impl {
    use super::*;
    #[test]
    fn filetype_new() {
        let segments = vec![String::from("tbfs"), String::from("testing")];
        insta::assert_debug_snapshot!(Filetype::new(segments), @r###"
    Filetype(
        [
            "tbfs",
            "testing",
        ],
    )
    "###);
    }

    #[test]
    fn filetype_is_text() {
        let filetype = Filetype::try_from("text::plain").unwrap();
        insta::assert_debug_snapshot!(filetype.is_text(), @"true");
        let filetype = Filetype::try_from("image::png").unwrap();
        insta::assert_debug_snapshot!(filetype.is_text(), @"false");
    }

    #[test]
    fn filetype_is_image() {
        let filetype = Filetype::try_from("text::plain").unwrap();
        insta::assert_debug_snapshot!(filetype.is_image(), @"false");
        let filetype = Filetype::try_from("image::png").unwrap();
        insta::assert_debug_snapshot!(filetype.is_image(), @"true");
    }

    #[test]
    fn filetype_is_internal() {
        let filetype = Filetype::try_from("text::plain").unwrap();
        insta::assert_debug_snapshot!(filetype.is_internal(), @"false");
        let filetype = Filetype::try_from("tbfs::internal::tag::simple").unwrap();
        insta::assert_debug_snapshot!(filetype.is_internal(), @"true");
        let filetype = Filetype::try_from("tbfs::other::tag::simple").unwrap();
        insta::assert_debug_snapshot!(filetype.is_internal(), @"false");
    }
}

impl std::cmp::PartialOrd for Filetype {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.to_string().cmp(&other.to_string()))
    }
}

impl std::cmp::Ord for Filetype {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.to_string().cmp(&other.to_string())
    }
}

#[test]
fn filetype_sorting() {
    let mut filetypes = vec![
        Filetype::from("text::plain"),
        Filetype::from("tbfs::internal::tag::complex::association"),
        Filetype::from("image::png"),
        Filetype::from("tbfs::internal::tag::simple"),
        Filetype::from("text::utf8"),
        Filetype::from("tbfs::internal::tag::complex::core"),
    ];
    filetypes.sort();
    insta::assert_debug_snapshot!(filetypes.iter().map(std::string::ToString::to_string).collect::<Vec<String>>(), @r###"
    [
        "image::png",
        "tbfs::internal::tag::complex::association",
        "tbfs::internal::tag::complex::core",
        "tbfs::internal::tag::simple",
        "text::plain",
        "text::utf8",
    ]
    "###);
}

impl TryFrom<IVec> for Filetype {
    type Error = FromUtf8Error;

    fn try_from(value: IVec) -> Result<Self, Self::Error> {
        let string: String = String::from_utf8(value.to_vec())?;
        let parts = string.split("::");
        let segments = parts.map(String::from).collect::<Vec<_>>();
        Ok(Self(segments))
    }
}

impl From<&Filetype> for IVec {
    fn from(f: &Filetype) -> Self {
        f.to_string().as_bytes().into()
    }
}

impl std::fmt::Display for Filetype {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(first) = self.0.first() {
            write!(f, "{first}")?;
            let rest = self.0.iter().skip(1);
            for segment in rest {
                write!(f, "::{segment}")?;
            }
        }
        Ok(())
    }
}

#[test]
fn filetype_display() {
    let filetype = Filetype::new(vec![
        String::from("tbfs"),
        String::from("internal"),
        String::from("tag"),
        String::from("complex"),
        String::from("association"),
    ]);
    insta::assert_debug_snapshot!(filetype.to_string(), @r###""tbfs::internal::tag::complex::association""###);
}

impl From<&str> for Filetype {
    /// This will also convert a MIME-style filetype, by replacing `/` and `+` by `::`.
    fn from(s: &str) -> Self {
        Self(
            s.replace(['/', '+'], "::")
                .split("::")
                .map(String::from)
                .collect(),
        )
    }
}

#[test]
fn filetype_from_str() {
    insta::assert_display_snapshot!(Filetype::from("tbfs::internal::tag::complex"), @"tbfs::internal::tag::complex");
    insta::assert_display_snapshot!(Filetype::from("text/plain"), @"text::plain");
    insta::assert_display_snapshot!(Filetype::from("image/png+bytes"), @"image::png::bytes");
    insta::assert_display_snapshot!(Filetype::from("text::plain/rust"), @"text::plain::rust");
    insta::assert_display_snapshot!(Filetype::from("text/utf8::rust+long lines"), @"text::utf8::rust::long lines");
}

/// Hash of a single [`File`].
///
/// Relevant section:
/// <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies#choice-of-hash-function/>
///
/// This implementation uses exclusively the SHA-3 algorithm.
///
/// The [`Filetype`] is not included in the hash calculation, this is to allow
/// for a file's filetype to be updated without producing a new file.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Hash(GenericArray<u8, generic_array::typenum::U32>);

#[cfg(test)]
impl serde::Serialize for Hash {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

#[cfg(test)]
impl<'de> serde::Deserialize<'de> for Hash {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s: String = serde::Deserialize::deserialize(deserializer)?;
        Ok(Self::try_from(base16ct::lower::decode_vec(s.as_bytes()).unwrap()).unwrap())
    }
}

impl Encode for Hash {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        bincode::Encode::encode(&self.0.to_vec(), encoder)
    }
}

impl Decode for Hash {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        let v: Vec<u8> = bincode::Decode::decode(decoder)?;
        let len = v.len();
        Self::try_from(v).map_err(|_| bincode::error::DecodeError::ArrayLengthMismatch {
            required: 32,
            found: len,
        })
    }
}

bincode::impl_borrow_decode!(Hash);

impl TryFrom<Vec<u8>> for Hash {
    type Error = crate::error::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        let arr: GenericArray<u8, _> = generic_array::GenericArray::from_exact_iter(value.iter())
            .ok_or_else(|| crate::error::Error::InvalidHash(value.clone()))?
            .map(|x| *x);
        Ok(Self(arr))
    }
}

impl TryFrom<sled::IVec> for Hash {
    type Error = crate::error::Error;

    fn try_from(value: sled::IVec) -> Result<Self, Self::Error> {
        let arr: GenericArray<u8, _> = generic_array::GenericArray::from_exact_iter(value.iter())
            .ok_or_else(|| crate::error::Error::InvalidHash(value.to_vec()))?
            .map(|x| *x);
        Ok(Self(arr))
    }
}

impl std::fmt::Display for Hash {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let buf = base16ct::lower::encode_string(&self.0);
        write!(f, "{buf}")
    }
}

impl AsRef<[u8]> for Hash {
    fn as_ref(&self) -> &[u8] {
        &self.0[..]
    }
}

/// Single [`File`].
///
/// Relevant section:
/// <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies#immutable-file-objects-with-hash-identities/>
#[derive(Debug, Clone, Eq, PartialEq)]
#[cfg_attr(test, derive(serde::Serialize, serde::Deserialize))]
pub struct File {
    /// Actual data content of the file.
    ///
    /// This data *should* be correct according to the associated filetype,
    /// however this should, if at all, be enforced by the client.
    data: Vec<u8>,
    /// Metadata for this file.
    ///
    /// Contains size, hash and filetype.
    metadata: Metadata,
}

impl File {
    /// Get non-mutable access to the hash of this file
    pub const fn hash(&self) -> &Hash {
        &self.metadata.hash
    }

    /// Get non-mutable access to the data of this file
    pub fn data(&self) -> &[u8] {
        &self.data
    }

    /// Get non-mutable access to the filetype of this file
    pub const fn filetype(&self) -> &Filetype {
        &self.metadata.filetype
    }

    /// Get non-mutable access to the metadata of this file
    pub const fn metadata(&self) -> &Metadata {
        &self.metadata
    }

    /// Create file object from data and filetype.
    ///
    /// Automatically computes the hash.
    ///
    /// Warning: does not save the file to disk.  Use an implementation of
    /// [`crate::storage::Storage`] for that.
    pub fn from_data<F: Into<Filetype>>(data: &[u8], filetype: F) -> Self {
        let hash = compute_hash(data);
        let len = data.len();
        let metadata = Metadata {
            hash,
            size_bytes: len,
            filetype: filetype.into(),
        };
        Self {
            data: data.to_vec(),
            metadata,
        }
    }

    /// Create a new file object from the given data, without checking that the
    /// hash and data are consistant.
    ///
    /// Warning: does not save the file to disk.  Use an implementation of
    /// [`crate::storage::Storage`] for that.
    pub(crate) fn new_unchecked(data: Vec<u8>, hash: Hash, filetype: Filetype) -> Self {
        let metadata = Metadata {
            hash,
            size_bytes: data.len(),
            filetype,
        };
        Self { data, metadata }
    }
}

#[test]
fn file_from_data() {
    insta::assert_yaml_snapshot!(File::from_data(b"This is some test data", "tbfs::testing"));
    insta::assert_yaml_snapshot!(File::from_data(
        b"This is some more test data",
        "tbfs::testing::text"
    ));
}

#[test]
fn file_new_unchecked() {
    let hash_bytes = base16ct::lower::decode_vec(
        b"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    )
    .unwrap();
    let hash = Hash::try_from(hash_bytes).unwrap();
    insta::assert_yaml_snapshot!(File::new_unchecked(
        vec![0xd, 0xe, 0xa, 0xd, 0xb, 0xe, 0xe, 0xf],
        hash.clone(),
        Filetype::from("tbfs::testing")
    ));
    insta::assert_yaml_snapshot!(File::new_unchecked(
        b"This is some other test data, that probably doesn't have a hash composed entirely of 'a's".to_vec(),
        hash,
        Filetype::from("tbfs::testing")
    ));
}

/// Compute the SHA-3 hash of the given data.
fn compute_hash(data: &[u8]) -> Hash {
    use sha3::Digest;

    let digest = sha3::Sha3_256::digest(data);
    Hash(digest)
}

#[test]
fn test_compute_hash() {
    let data = b"This is some test data";
    let hash = compute_hash(data);
    assert_eq!(
        hash.0,
        [
            106, 154, 196, 162, 223, 67, 172, 191, 148, 129, 199, 29, 69, 210, 128, 169, 246, 15,
            218, 146, 167, 179, 78, 57, 254, 168, 218, 249, 82, 167, 211, 228
        ]
        .into()
    );
}

#[test]
fn test_from_data() {
    let data = b"This is some more test data";
    let hash = compute_hash(data);
    let file = File::from_data(data, "text::plain");
    assert_eq!(&hash, file.hash());
}

/// Contains data needed to display a file's properties on the terminal.
#[derive(Debug, Clone, Eq, PartialEq)]
#[allow(clippy::module_name_repetitions)]
#[cfg_attr(test, derive(serde::Serialize, serde::Deserialize))]
pub struct Metadata {
    /// Hash of the data
    hash: Hash,
    /// Size of the file, in bytes
    size_bytes: usize,
    /// Name of the structure of the data.
    ///
    /// This implementation does not allow multiple filetypes per file, and as
    /// such if one file format has multiple valid filetypes, this situation
    /// will not be correctly represented by this implementation.
    filetype: Filetype,
}

/// Holds formatting options to format a [`Metadata`]
#[derive(Debug, Clone, Eq, PartialEq, Copy)]
#[allow(clippy::struct_excessive_bools)]
#[cfg_attr(test, derive(serde::Serialize, serde::Deserialize))]
pub struct MetadataFormatSpecifier {
    /// If true, show the full hash, else only show the first 20 characters
    pub full_hash: bool,
    /// If true, show the file size
    pub show_size: bool,
    /// If true, show the file size with human-friendly units, instead of raw bytes
    pub human_size: bool,
    /// If true, show the filetype
    pub show_filetype: bool,
}

impl Metadata {
    /// Create a new instance without checking that the data is correct.
    pub(crate) const fn new_unchecked(hash: Hash, size_bytes: usize, filetype: Filetype) -> Self {
        Self {
            hash,
            size_bytes,
            filetype,
        }
    }

    /// Get read-only reference to the hash
    pub const fn hash(&self) -> &Hash {
        &self.hash
    }

    /// Get read-only reference to the size
    pub const fn size(&self) -> usize {
        self.size_bytes
    }

    /// Get read-only reference to the filetype
    pub const fn filetype(&self) -> &Filetype {
        &self.filetype
    }

    /// Format the metadata in a tab-separated string using the given specifier.
    ///
    /// The format will look something like this:
    /// ```bash
    /// 0b7944666fd2b1e7b2a4    11 B    text::plain
    /// ```
    pub fn format(&self, specifier: MetadataFormatSpecifier) -> String {
        // this is repeated here because non-integration tests do not pass by main
        // Do not use colors if trying to test
        #[cfg(test)]
        colored::control::set_override(false);

        let mut out = String::new();

        let hash = if specifier.full_hash {
            format!("{}", self.hash.to_string().blue())
        } else {
            format!("{0:.20}", self.hash.to_string().blue())
        };
        out.push_str(&hash);

        if specifier.show_size {
            let size = if specifier.human_size {
                #[allow(clippy::cast_precision_loss)]
                human_bytes::human_bytes(self.size_bytes as f64)
            } else {
                self.size_bytes.to_string()
            };
            out.push(' ');
            out.push_str(&format!("{size:<10}"));
        }

        if specifier.show_filetype {
            let filetype = format!("{:<32}", self.filetype.to_string().yellow());
            out.push(' ');
            out.push_str(&filetype);
        }

        out
    }
}

#[test]
fn metadata_new_unchecked() {
    let hash = compute_hash(b"some data that is not 139 bytes long");
    let m = Metadata::new_unchecked(hash, 139, Filetype::from("tbfs::testing"));
    assert_yaml_snapshot!(m);
}

#[test]
fn metadata_format() {
    let m = Metadata::new_unchecked(
        compute_hash(b"some testing data"),
        5498,
        Filetype::from("tbfs::testing"),
    );
    let mut specifier = MetadataFormatSpecifier {
        full_hash: false,
        show_size: false,
        human_size: false,
        show_filetype: false,
    };

    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.full_hash = true;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.show_size = true;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.show_filetype = true;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.show_size = false;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.full_hash = false;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.human_size = true;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.show_size = true;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
    specifier.full_hash = true;
    assert_yaml_snapshot!((specifier, m.format(specifier)));
}
