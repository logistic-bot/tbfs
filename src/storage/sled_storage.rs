//! A [`Storage`] implementation using the sled database.

#[cfg(test)]
use crate::test_utils::add_default_files;
#[cfg(test)]
use crate::test_utils::{file1_hash, file2_hash};
#[cfg(test)]
use function_name::named;
#[cfg(test)]
use std::path::PathBuf;

use std::path::Path;

use crate::error::{Error, TbfsResult};
use crate::file::Metadata;
use crate::filetypes::name::ShortName;
use crate::filetypes::simple_tag::SimpleTag;
use crate::prelude::*;

use super::{InsertSuccess, Storage};

/// An implementation of [`Storage`] built on the [`sled`] crate.
///
/// While the sled crate is still in beta, I wanted to use a pure-rust solution.
/// The best solution would probably be to write my own storage backend, but I'm
/// too lazy for that, and if need (or motivation) for this arrises in the
/// future, I (will try to) make storage backends as interchangeable as possible.
///
/// This uses two [`sled::Db`]s, both using the hash of the file as key, and the
/// data and filetype as value.
///
/// This allows for a simplified design, not requiring manipulations of the keys
/// for separate storage of the filetypes and data.
#[allow(clippy::module_name_repetitions)]
pub struct SledStorage {
    /// Database for storing files
    files: sled::Db,
    /// Database for storing filetypes
    filetypes: sled::Db,
}

impl SledStorage {
    /// Create a new [`SledStorage`] at the given path on disk, creating it if
    /// it doesn't exist.
    ///
    /// # Errors
    ///
    /// This function may return an error if:
    /// - creating the database directory fails
    /// - creating or opening the sled databases fails
    pub fn new<P: AsRef<Path>>(base_path: P) -> TbfsResult<Self> {
        Self::_new(base_path.as_ref())
    }

    /// This is the actual [`SledStorage::new`] function, it is separated to
    /// prevent too many machine instructions to be generated for all the
    /// different values of the [`Path`] generic argument.
    fn _new(base_path: &Path) -> TbfsResult<Self> {
        std::fs::create_dir_all(base_path)?;

        let files = sled::open(base_path.join("files"))?;
        let filetypes = sled::open(base_path.join("filetypes"))?;

        Ok(Self { files, filetypes })
    }
}

impl Storage for SledStorage {
    fn insert(&mut self, file: &File) -> TbfsResult<InsertSuccess> {
        self.filetypes.insert(file.hash(), file.filetype())?;
        let old_value = self.files.insert(file.hash(), file.data())?;
        if old_value.is_some() {
            Ok(InsertSuccess::ExistingFile)
        } else {
            Ok(InsertSuccess::NewFile)
        }
    }

    fn delete(&mut self, hash: &Hash) -> TbfsResult<File> {
        let file = self.get(hash)?;
        self.filetypes.remove(hash)?;
        self.files.remove(hash)?;
        Ok(file)
    }

    fn get(&self, hash: &Hash) -> TbfsResult<File> {
        let data = self
            .files
            .get(hash)?
            .ok_or_else(|| Error::FileDoesNotExist(hash.clone()))?
            .to_vec();
        let filetype = {
            let filetype = self
                .filetypes
                .get(hash)?
                .ok_or_else(|| Error::FileDoesNotExist(hash.clone()))?;
            filetype.try_into().map_err(Error::CorruptedFiletype)?
        };
        let hash = hash.clone();

        let file = File::new_unchecked(data, hash, filetype);
        Ok(file)
    }

    fn list(&self) -> TbfsResult<Vec<(Hash, Filetype)>> {
        let items = self.filetypes.iter();
        let mut final_items: Vec<(Hash, Filetype)> = Vec::new();
        for item in items {
            let item = item?;
            let (hash, filetype) = (
                Hash::try_from(item.0)?,
                Filetype::try_from(item.1.clone())
                    .map_err(|_x| Error::InvalidHash(item.1.to_vec()))?,
            );
            final_items.push((hash, filetype));
        }
        Ok(final_items)
    }

    fn get_metadata(&self, hash: &Hash) -> TbfsResult<Metadata> {
        Ok(Metadata::new_unchecked(
            hash.clone(),
            self.get_size(hash)?,
            self.get_filetype(hash)?,
        ))
    }

    fn get_size(&self, hash: &Hash) -> TbfsResult<usize> {
        let file = self.files.get(hash.as_ref())?;
        if let Some(file) = file {
            Ok(file.len())
        } else {
            Err(Error::FileDoesNotExist(hash.clone()))
        }
    }

    fn get_filetype(&self, hash: &Hash) -> TbfsResult<Filetype> {
        let raw_filetype = self
            .filetypes
            .get(hash)?
            .ok_or_else(|| Error::FileDoesNotExist(hash.clone()))?;
        let filetype = Filetype::try_from(raw_filetype).map_err(Error::CorruptedFiletype)?;
        Ok(filetype)
    }

    fn get_canonical_hash(&self, hash: &str) -> TbfsResult<Option<Hash>> {
        let hash_bytes = base16ct::mixed::decode_vec(hash)?;
        let res = self.filetypes.scan_prefix(&hash_bytes);
        let keys = res
            .keys()
            .collect::<Result<Vec<sled::IVec>, _>>()?
            .into_iter()
            .map(Hash::try_from)
            .collect::<Result<Vec<Hash>, _>>()?;
        match keys.first() {
            None => Ok(None),
            Some(key) if keys.len() == 1 => Ok(Some(key.clone())),
            Some(_) => Err(Error::AmbiguousPartialHash(hash_bytes, keys)),
        }
    }

    fn get_all_hashes(&self) -> TbfsResult<Vec<Hash>> {
        let items = self.filetypes.iter();
        let mut final_items: Vec<Hash> = Vec::new();
        for item in items {
            let item = item?;
            let hash = Hash::try_from(item.0)?;
            final_items.push(hash);
        }
        Ok(final_items)
    }

    fn get_all_metadatas(&self) -> TbfsResult<Vec<Metadata>> {
        let hashes = self.get_all_hashes()?;
        let mut final_result = Vec::with_capacity(hashes.len());
        for hash in hashes {
            let diplay_file = self.get_metadata(&hash)?;
            final_result.push(diplay_file);
        }
        Ok(final_result)
    }

    fn get_files_of_type(&self, target_filetype: &Filetype) -> TbfsResult<Vec<File>> {
        let mut result = Vec::new();

        let filetypes = self.filetypes.iter();
        for item in filetypes {
            let (raw_hash, raw_filetype) = item?;

            let filetype = Filetype::try_from(raw_filetype)
                .map_err(|_x| Error::InvalidHash(raw_hash.to_vec()))?;

            if &filetype == target_filetype {
                let hash = Hash::try_from(raw_hash)?;
                let found_file = self.get(&hash)?;
                result.push(found_file);
            }
        }
        Ok(result)
    }

    fn find_simple_tags(&self, target_hash: &Hash) -> TbfsResult<Vec<(Hash, SimpleTag)>> {
        let tag_files = self.get_files_of_type(&crate::filetypes::simple_tag::filetype())?;

        let mut result = Vec::new();
        for tag in tag_files {
            let hash = tag.hash().clone();
            let tag = SimpleTag::try_from(tag)?;
            if &tag.target_file == target_hash {
                result.push((hash, tag));
            }
        }
        Ok(result)
    }

    fn find_short_names(
        &self,
        target_hash: &Hash,
    ) -> TbfsResult<Vec<(Hash, crate::filetypes::name::ShortName)>> {
        let short_name_files = self.get_files_of_type(&crate::filetypes::name::filetype())?;

        let mut result = Vec::new();
        for name in short_name_files {
            let hash = name.hash().clone();
            let name = ShortName::try_from(name)?;
            if &name.target_file == target_hash {
                result.push((hash, name));
            }
        }
        Ok(result)
    }

    fn total_size_on_disk(&self) -> TbfsResult<u64> {
        Ok(self.filetypes.size_on_disk()? + self.files.size_on_disk()?)
    }
}
#[test]
fn get_canonical_hash() {
    let mut store = SledStorage::new(test_path("get_canonical_hash")).unwrap();
    let file = File::from_data(
        b"This is a few words of test data",
        Filetype::from("text::plain"),
    );
    store.insert(&file).unwrap();

    let full_hash = &"644c4ab21398822b75a33ddd0cea871c773d5720acfdfa79acf77b90203a862c";
    let full_hash = Hash::try_from(base16ct::lower::decode_vec(full_hash).unwrap()).unwrap();

    assert_eq!(
        store.get_canonical_hash("64").unwrap(),
        Some(full_hash.clone())
    );
    assert_eq!(store.get_canonical_hash("44").unwrap(), None);

    let file = File::from_data(
        b"This is some more test dataf",
        Filetype::from("text::plain"),
    );
    store.insert(&file).unwrap();

    let full_hash2 = &"64708fc0685d4a266ad15a428aedd53982ceafa21f2ed6df282e5012b05ae5ff";
    let full_hash2 = Hash::try_from(base16ct::lower::decode_vec(full_hash2).unwrap()).unwrap();

    assert!(matches!(
        store.get_canonical_hash("64").unwrap_err(),
        Error::AmbiguousPartialHash(hash, matching_hashes) if hash == [100] && {
            let mut matching_hashes = matching_hashes.clone();
            matching_hashes.sort();
            matching_hashes == vec![full_hash, full_hash2]
        }
    ));
    assert_eq!(store.get_canonical_hash("44").unwrap(), None);
}

#[cfg(test)]
fn test_path(name: &str) -> PathBuf {
    let path = Path::new(env!("CARGO_MANIFEST_DIR"))
        .join("tbfs_store")
        .join(name);
    let _ignore = std::fs::remove_dir_all(&path);
    std::fs::create_dir_all(&path).unwrap();
    path
}

#[test]
fn save_and_retrive_single_session() {
    // Initialize the store
    let base_path = test_path("save_and_retrive_single_session");

    let mut store = SledStorage::new(base_path).unwrap();
    // Create a file object
    let file = File::from_data(b"This is some data", Filetype::from("text::plain"));

    // Save the object to the store
    store.insert(&file).unwrap();

    // Get the file
    let hash = file.hash();
    let retrieved_file = store.get(hash).unwrap();

    assert_eq!(file, retrieved_file);
}

#[test]
fn insert_large_object() {
    // Initialize the store
    let base_path = test_path("insert_large_object");

    let mut store = SledStorage::new(base_path).unwrap();
    // Create a file object
    let data = include_bytes!("../../target/debug/tbfs"); // TODO: find a better way to test with large files
    let file = File::from_data(data, Filetype::from("text::plain"));

    // Save the object to the store
    store.insert(&file).unwrap();

    // Get the file
    let hash = file.hash();
    let retrieved_file = store.get(hash).unwrap();

    assert_eq!(file, retrieved_file);
}

#[test]
#[named]
fn find_no_short_name() -> color_eyre::Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files(&path)?;

    let result = store.find_short_names(&file1_hash()?)?;

    insta::assert_yaml_snapshot!(result);

    Ok(())
}

#[test]
#[named]
fn find_single_short_name() -> color_eyre::Result<()> {
    let path = test_path(function_name!());
    let mut store = add_default_files(&path)?;

    let name = ShortName::new(file2_hash()?, "File #2".to_string());
    let name_file = File::try_from(name)?;
    store.insert(&name_file)?;

    let result = store.find_short_names(&file2_hash()?)?;

    insta::assert_yaml_snapshot!(result);

    Ok(())
}

#[test]
#[named]
fn find_multiple_short_names() -> color_eyre::Result<()> {
    let path = test_path(function_name!());
    let mut store = add_default_files(&path)?;

    let name = ShortName::new(file2_hash()?, "File #2".to_string());
    let name_file = File::try_from(name)?;
    store.insert(&name_file)?;
    let name = ShortName::new(file2_hash()?, "Another name for file #2".to_string());
    let name_file = File::try_from(name)?;
    store.insert(&name_file)?;

    let result = store.find_short_names(&file2_hash()?)?;

    insta::assert_yaml_snapshot!(result);

    Ok(())
}
