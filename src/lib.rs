//! This is an attempt (I think it's about number 5 now. Sigh.) to implement
//! Nayuki's proposed tag based file system.
//!
//! Link to the original brainstorm document:
//! <https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies/>

#![feature(error_generic_member_access)]
#![feature(provide_any)]

pub mod error;
pub mod file;
pub mod filetypes;
pub mod prelude;
pub mod storage;

// We can not mark this as cfg(test)
// see https://github.com/rust-lang/cargo/issues/8379
// #[cfg(test)]
#[doc(hidden)]
pub mod test_utils;
