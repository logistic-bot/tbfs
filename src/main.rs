//! Command-line interface to this TBFS implementation.

#![warn(clippy::unwrap_used)]
#![warn(clippy::expect_used)]

use std::{
    io::{Read, Write},
    path::PathBuf,
};

use clap::{Parser, ValueEnum};
use color_eyre::eyre::{eyre, Context};
use color_eyre::{Help, Result};
use colored::Colorize;

use regex::Regex;
use tbfs::{
    file::MetadataFormatSpecifier,
    filetypes::{self, name::ShortName, simple_tag::SimpleTag},
    prelude::*,
    storage::{InsertSuccess, SledStorage, Storage},
};

/// TBFS -- Tag Based File System
///
/// Original design by Nayuki. Original implementation by Khaïs COLIN.
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// Subcommand to run
    #[command(subcommand)]
    action: Action,
    /// Specify location of the data store.
    /// Defaults to ~/.local/share/tbfs.
    #[arg(long)]
    datastore: Option<PathBuf>,
    /// Specify location of the tmp folder.
    /// Defaults to ~/.cache/tbfs.
    #[arg(long)]
    tmp_directory: Option<PathBuf>,
}

/// Action to take
#[derive(clap::Subcommand, Debug)]
enum Action {
    /// Insert new file into the store
    Insert {
        /// Files to insert into the store
        #[arg(value_name = "SRC", required = true)]
        source: Vec<PathBuf>,
        /// Filetype of the new files.
        ///
        /// If nothing is specified, tries to detect MIME type for each file. If that fails, defaults to `data`.
        #[arg(value_name = "FILETYPE", last = true)]
        filetype: Option<String>,
    },
    /// Delete files from the store
    ///
    /// WARNING: There is ABSOLUTELY NO CONFIRMATION
    Delete {
        #[arg(value_name = "HASH", required = true)]
        hashes: Vec<String>,
    },
    /// Copy a file from the store into DEST
    Get {
        /// Hash of the file to get
        #[arg(value_name = "HASH")]
        hash: String,
        /// Destination file name. Defaults to the hash of the file.
        #[arg(value_name = "DEST")]
        destination: Option<PathBuf>,
    },
    /// List files from the store
    List {
        /// Also show the filetype. Default true
        #[arg(short, long)]
        filetype: Option<bool>,
        /// Also show the size. Default true
        #[arg(short, long)]
        size: Option<bool>,
        /// Show the size in human-readable format. Default true
        #[arg(short = 'H', long)]
        human: Option<bool>,
        /// Show the full hash, instead of only the first 20 chars
        #[arg(short = 'F', long)]
        full_hash: bool,
        /// Sort the files by the given information
        #[arg(short = 'S', long)]
        sort: Option<Sort>,
        /// Show short names of files
        #[arg(long, short = 'N')]
        show_short_names: bool,
    },
    /// Decode and show a file, in the terminal if appropriate, else in external program
    Show {
        /// Hash of the file to show
        #[arg(value_name = "HASH")]
        hash: String,
        /// Show image files inside of the terminal.
        ///
        /// If unspecified, will only embed images if your terminal supports the
        /// kitty or iterm image embedding protocol.
        #[arg(long)]
        embed_images: Option<bool>,
    },
    /// Manipulate simple tags.
    ///
    /// Simple tags are a simple association between some text and a file.
    ///
    /// If multiple files are tagged with the same text, the text will be stored
    /// once for each file, which makes them inefficient for common tags
    ///
    /// These should be used when it is unlikely that many files will have the same
    /// tag, and the tag models a *string* and not a *concept*. when modeling a
    /// *concept*, such as "this image depicts a human" please use complex tags.
    ///
    /// One usage of these is to keep track of the original filepath from
    /// imported files. Since each imported file will have a different filepath,
    /// this is a perfect usage for simple tags. In that case, the simple tag is
    /// then tagged with a marker denoting that it is the original filepath.
    Tag {
        /// Tag command to run
        #[command(subcommand)]
        action: TagAction,
    },
    /// Manipulate short names.
    ///
    /// Short names are very similar to simple tags, with the same limitations,
    /// but they are used when you want to name a file, instead of describing
    /// what is in the file.
    ///
    /// As with simple tags, in most cases you want to use a complex indirect
    /// tag, and name the tag core.
    Name {
        /// Name command to run
        #[command(subcommand)]
        action: NameAction,
    },
}

/// Action relating to simple tags
#[derive(clap::Subcommand, Debug)]
enum TagAction {
    /// Create a new simple tag
    Add {
        /// Files to be tagged with the given string.
        ///
        /// A different tag will be created for each file.
        ///
        /// Note that the string will need to be stored once per tagged file. To
        /// be more efficient, consider using complex tags.
        #[arg(value_name = "TARGET", required = false)]
        targets: Vec<String>,
        /// The string to be associated with these files.
        #[arg(value_name = "CONTENT", last = true)]
        content: String,
    },
    /// Get tags for a file
    Get {
        /// File to search for tags
        #[arg(required = true)]
        hash: String,
    },
    /// Find files tagged with given regex
    Search {
        /// Regex to match tags against.
        #[arg(required = true)]
        regex: String,
    },
}

/// Actions relating to short names
#[derive(clap::Subcommand, Debug)]
enum NameAction {
    /// Assign a new name to a file.
    /// Multiple names can be assigned to the same file.
    ///
    /// Reminder: In most cases you want to use complex indirect tags.
    Add {
        /// File to be named.
        ///
        /// This intentionally does not support naming multiple files at the
        /// same time.  If you want to do that, you probably want complex
        /// indirect tags.
        #[arg(value_name = "TARGET", required = true)]
        target: String,
        /// The name to associate with this file
        #[arg(value_name = "NAME", required = true)]
        name: String,
    },
    /// Get short names for files.
    Get {
        /// Files to get names for.
        /// When not provided, get names for all files.
        #[arg(value_name = "HASH", required = false)]
        targets: Vec<String>,
    },
}

#[derive(Clone, Debug, ValueEnum)]
enum Sort {
    Size,
    Hash,
    Filetype,
}

fn main() -> Result<()> {
    // hide spantrace (line numbers) by default
    if std::env::var("RUST_SPANTRACE").is_err() {
        std::env::set_var("RUST_SPANTRACE", "0");
        color_eyre::config::HookBuilder::default()
            .display_location_section(false)
            .display_env_section(false)
            .install()?;
    } else {
        color_eyre::install()?;
    }

    // Do not use colors if trying to test
    #[cfg(test)]
    colored::control::set_override(false);

    let args = Args::parse();

    let datastore_path = get_datastore_path(&args)?;

    let mut store = SledStorage::new(&datastore_path)
        .wrap_err_with(|| format!("Failed to open datastore at {}", datastore_path.display()))
        .suggestion("Ensure you have write access to that directory")?;

    match args.action {
        Action::Insert { source, filetype } => action_insert(&mut store, &source, filetype),
        Action::Delete { hashes } => action_delete(&mut store, &hashes),
        Action::Get { hash, destination } => action_get(&store, &hash, destination),
        Action::List {
            filetype,
            size,
            human,
            full_hash,
            sort,
            show_short_names,
        } => {
            let specifier = MetadataFormatSpecifier {
                full_hash,
                show_size: size.unwrap_or(true),
                human_size: human.unwrap_or(true),
                show_filetype: filetype.unwrap_or(true),
            };
            let sort = sort.unwrap_or(Sort::Hash);
            action_list(&store, specifier, &sort, show_short_names)
        }
        Action::Show { hash, embed_images } => {
            action_show(&store, &hash, &args.tmp_directory, embed_images)
        }
        Action::Tag { action } => action_tag(action, store),
        Action::Name { action } => action_name(action, &mut store),
    }
}

fn action_name(action: NameAction, store: &mut SledStorage) -> Result<()> {
    match action {
        NameAction::Add { target, name } => action_name_add(store, &target, name),
        NameAction::Get { targets } => action_name_get(store, targets),
    }
}

fn action_name_get(store: &mut SledStorage, targets: Vec<String>) -> Result<()> {
    let mut succeeded = 0;
    let mut failed = 0;
    let mut names = 0;

    let target_hashes = if targets.is_empty() {
        store
            .list()?
            .iter_mut()
            .map(|x| x.0.clone())
            .collect::<Vec<_>>()
    } else {
        let mut target_hashes = Vec::new();
        for target in targets {
            let maybe_target_hash = store
                .get_canonical_hash(&target)
                .wrap_err(format!("Failed to get canonical hash for '{target}'"));
            match maybe_target_hash {
                Ok(target_hash) => {
                    if let Some(target_hash) = target_hash {
                        target_hashes.push(target_hash);
                    } else {
                        let e = eyre!("No file has this hash: {target}");
                        println!("{} {e:#}", "Error:".red());
                        failed += 1;
                    };
                }
                Err(e) => {
                    println!("{} {e:#}", "Error:".red());
                    failed += 1;
                }
            }
        }
        target_hashes
    };

    for target_hash in target_hashes {
        let result = action_name_get_single_file(store, &target_hash);
        match result {
            Ok(names_displayed) => {
                names += names_displayed;
                succeeded += 1;
            }
            Err(e) => {
                println!("{e:#}");
                failed += 1;
            }
        }
    }

    println!(
        "Processed {} file{} ({} {} {} {}) and {} name{}",
        succeeded + failed,
        if succeeded + failed == 1 { "" } else { "s" },
        succeeded,
        "succeeded".green(),
        failed,
        "failed".red(),
        names,
        if names == 1 { "" } else { "s" },
    );

    if failed > 0 {
        Err(eyre!(
            "Some operations failed. See above output for details."
        ))
    } else {
        Ok(())
    }
}

fn action_name_get_single_file(
    store: &mut SledStorage,
    target_hash: &Hash,
) -> Result<usize, color_eyre::Report> {
    print!("{} {}", "==>".blue().bold(), target_hash.to_string().blue());
    std::io::stdout().flush()?;
    let file = store
        .get(target_hash)
        .wrap_err("Failed to get file from store")?;
    println!(" {}", file.filetype().to_string().yellow());

    let names = store
        .find_short_names(target_hash)
        .wrap_err("Failed to find names")?;
    for (hash, name) in &names {
        println!(
            "{} {} {}",
            " ::".blue(),
            hash.to_string().magenta().dimmed(),
            tbfs::filetypes::name::FILETYPE_STR.yellow()
        );
        for line in name.name.lines() {
            println!("    {line}");
        }
    }

    Ok(names.len())
}

fn action_name_add(store: &mut SledStorage, target_hash: &str, name: String) -> Result<()> {
    let target = store
        .get_canonical_hash(target_hash)
        .wrap_err("Failed to get canonical hash")?;
    let target = if let Some(target) = target {
        Ok(target)
    } else {
        Err(eyre!("No file has this hash: {target_hash}"))
    }?;
    let name_file_repr = ShortName::new(target.clone(), name);
    let name_file =
        File::try_from(name_file_repr.clone()).wrap_err("Failed to encode file name")?;
    let hash = name_file.hash();
    let res = store
        .insert(&name_file)
        .wrap_err("Failed to insert file name file")?;
    match res {
        InsertSuccess::ExistingFile => {
            println!(
                "{} {} <-- {}",
                "OK ".green(),
                target.to_string().blue(),
                &hash.to_string().cyan(),
            );
            println!("{}", name_file_repr.name);
        }
        InsertSuccess::NewFile => {
            println!(
                "{} {} <-- {}",
                "NEW".green(),
                target.to_string().blue(),
                &hash.to_string().green(),
            );
            println!("{}", name_file_repr.name);
        }
    }
    Ok(())
}

fn action_tag(action: TagAction, store: SledStorage) -> Result<()> {
    match action {
        TagAction::Add { targets, content } => action_tag_add(&targets, store, &content),
        TagAction::Get { hash } => action_tag_get(&store, &hash),
        TagAction::Search { regex } => action_tag_search(&store, &regex),
    }
}

fn action_tag_search(store: &SledStorage, regex: &str) -> Result<()> {
    let mut final_regex = String::new();
    final_regex.push('^');
    final_regex.push_str(regex);
    final_regex.push('$');
    let re = Regex::new(&final_regex).wrap_err("Invalid regular expression")?;

    let all_tags = store
        .get_files_of_type(&filetypes::simple_tag::filetype())
        .wrap_err("Failed to get list of all simple tags")?;
    let mut shown_files = Vec::new();
    for tag_file in all_tags {
        let tag = SimpleTag::try_from(tag_file)
            .wrap_err("Invalid simple tag")
            .suggestion("Correct the filetype for this file and try again")?;
        if re.is_match(&tag.content) {
            let target_file = tag.target_file;
            if !shown_files.contains(&target_file) {
                shown_files.push(target_file.clone());
                println!("{}", target_file.to_string().green());
            }
        }
    }
    Ok(())
}

fn action_tag_get(store: &SledStorage, target_hash: &str) -> Result<()> {
    let target = store
        .get_canonical_hash(target_hash)
        .wrap_err("Failed to get canonical hash")?;
    let target = if let Some(target) = target {
        Ok(target)
    } else {
        Err(eyre!("No file has this hash: {target_hash}"))
    }?;
    let tags = store
        .find_simple_tags(&target)
        .wrap_err("Failed to find simple tags")?;
    for tag_hash in tags {
        println!("{}", tag_hash.0.to_string().blue());
        println!("{}", tag_hash.1.content);
    }
    Ok(())
}

fn action_tag_add(targets: &[String], mut store: SledStorage, content: &str) -> Result<()> {
    let mut succeded = 0;
    let mut failed = 0;
    for target_hash in targets {
        let target = store
            .get_canonical_hash(target_hash)
            .wrap_err("Failed to get canonical hash")?;
        let target = if let Some(target) = target {
            Ok(target)
        } else {
            Err(eyre!("No file has this hash: {target_hash}"))
        }?;
        match add_single_tag(&mut store, &target, content) {
            Ok(_) => {
                succeded += 1;
            }
            Err(e) => {
                let e = e.wrap_err(format!("While adding tag to {target_hash}"));
                println!("{e:#}");
                failed += 1;
            }
        }
    }

    println!(
        "Processed {} files ({succeded} {} {failed} {})",
        targets.len(),
        "succeeded".green(),
        "failed".red()
    );

    Ok(())
}

fn add_single_tag(store: &mut SledStorage, target: &Hash, content: &str) -> Result<Hash> {
    let tag_file = File::try_from(SimpleTag::new(target.clone(), content.to_string()))
        .wrap_err("Failed to create in-memory tag file")?;
    let res = store
        .insert(&tag_file)
        .wrap_err("Failed to insert tag file")?;

    let hash = tag_file.hash().clone();
    match res {
        InsertSuccess::ExistingFile => {
            println!(
                "{} {} <-- {}",
                "OK ".green(),
                target.to_string().blue(),
                &hash.to_string().cyan()
            );
        }
        InsertSuccess::NewFile => {
            println!(
                "{} {} <-- {}",
                "NEW".green(),
                target.to_string().blue(),
                &hash.to_string().green()
            );
        }
    }

    Ok(hash)
}

fn action_show(
    store: &SledStorage,
    hash: &str,
    tmp_directory: &Option<PathBuf>,
    embed_images: Option<bool>,
) -> Result<()> {
    let canonical_hash = store
        .get_canonical_hash(hash)
        .wrap_err("Failed to get canonical hash")?;
    if let Some(hash) = canonical_hash {
        print!("{} ", hash.to_string().blue());
        let file = store
            .get(&hash)
            .wrap_err("Failed to get file from datastore")?;

        let filetype = file.filetype();
        println!("{}", filetype.to_string().yellow());
        if filetype.is_text() {
            print!("{}", String::from_utf8_lossy(file.data()));
            Ok(())
        } else if filetype.is_internal() {
            if filetype == &tbfs::filetypes::simple_tag::filetype() {
                let tag = SimpleTag::try_from(file)
                    .wrap_err("Failed to decode simple tag from file")
                    .suggestion("Either the file is corrupted, or it has an incorrect filetype")?;
                println!("{tag}");
                Ok(())
            } else if filetype == &tbfs::filetypes::name::filetype() {
                let name = ShortName::try_from(file)
                    .wrap_err("Failed to decode short name from file")
                    .suggestion("Either the file is corrupted, or it has an incorrect filetype")?;
                println!("{name}");
                Ok(())
            } else {
                Err(eyre!("Unknown internal filetype {filetype}"))
                    .suggestion("Correct the filetype to one of the supported filetypes")
                    .suggestion("If you wish to see the file anyway, export it with tbfs get")
            }
        } else {
            let mut tmp_file_path = get_tmp_path(tmp_directory)?;
            tmp_file_path.push(hash.to_string());

            let mut tmp_file =
                std::fs::File::create(&tmp_file_path).wrap_err("Failed to create tmp file")?;
            tmp_file
                .write_all(file.data())
                .wrap_err("Failed to write data to tmp file")?;
            if file.filetype().is_image() {
                show_image(embed_images, &tmp_file_path)
            } else {
                open::that(&tmp_file_path).wrap_err(format!(
                    "Failed to open resulting file {}",
                    tmp_file_path.display()
                ))
            }
        }
    } else {
        Err(eyre!("No file has this hash: {hash}"))
    }
}

fn show_image(embed_images: Option<bool>, tmp_file_path: &PathBuf) -> Result<()> {
    let embed_image = match embed_images {
        None => {
            viuer::is_iterm_supported()
                || !matches!(viuer::get_kitty_support(), viuer::KittySupport::None)
        }
        Some(x) => x,
    };
    if embed_image {
        let res = viuer::print_from_file(
            tmp_file_path,
            &viuer::Config {
                transparent: false,
                absolute_offset: false,
                ..Default::default()
            },
        );
        if res.is_err() {
            println!("{}", "WARNING: failed to show image in terminal".yellow());
            open::that(tmp_file_path).wrap_err(format!(
                "Failed to open resulting file {}",
                tmp_file_path.display()
            ))
        } else {
            Ok(())
        }
    } else {
        open::that(tmp_file_path).wrap_err(format!(
            "Failed to open resulting file {}",
            tmp_file_path.display()
        ))
    }
}

fn action_list(
    store: &SledStorage,
    specifier: MetadataFormatSpecifier,
    sort: &Sort,
    show_short_names: bool,
) -> Result<()> {
    let mut items = store
        .get_all_metadatas()
        .wrap_err("Failed to list files from store")?;
    items.sort_by(|file_a, file_b| match sort {
        Sort::Size => file_a.size().cmp(&file_b.size()),
        Sort::Hash => file_a.hash().cmp(file_b.hash()),
        Sort::Filetype => file_a.filetype().cmp(file_b.filetype()),
    });

    if specifier.full_hash {
        print!("{:<64}", "hash".bold());
    } else {
        print!("{:<20}", "hash".bold());
    }
    if specifier.show_size {
        print!(" {:<10}", "size".bold());
    }
    if specifier.show_filetype {
        print!(" {}", "filetype".bold());
    }
    println!();
    for item in &items {
        print!("{}", item.format(specifier));
        if show_short_names {
            let mut short_names = store
                .find_short_names(item.hash())
                .wrap_err("Failed to find short names for file")?;
            if let Some(name) = short_names.pop() {
                if let Some(name) = name.1.name.lines().next() {
                    print!("{name}");
                }
            }
            for name in short_names {
                if let Some(name) = name.1.name.lines().next() {
                    print!("{} {}", ";".green().bold(), name);
                }
            }
        }
        println!();
    }

    #[allow(clippy::cast_precision_loss)]
    let total_size =
        human_bytes::human_bytes(items.iter().fold(0, |x, item| x + item.size()) as f64);
    #[allow(clippy::cast_precision_loss)]
    let total_size_on_disk = human_bytes::human_bytes(store.total_size_on_disk()? as f64);

    println!(
        "{} item{} listed totaling {} ({} on disk)",
        items.len(),
        if items.len() > 1 { "s" } else { "" },
        total_size,
        total_size_on_disk
    );
    Ok(())
}

fn action_get(store: &SledStorage, hash: &str, destination: Option<PathBuf>) -> Result<()> {
    let canonical_hash = store
        .get_canonical_hash(hash)
        .wrap_err("Failed to get canonical hash")?;
    if let Some(hash) = canonical_hash {
        print!("{} ", hash.to_string().blue());
        std::io::stdout().flush()?;
        let file = store
            .get(&hash)
            .wrap_err("Failed to get file from datastore")?;
        println!("{}", file.filetype().to_string().yellow());
        let output_path = match destination {
            Some(dest) => dest,
            None => PathBuf::from(&hash.to_string()),
        };
        let mut output_file =
            std::fs::File::create(output_path).wrap_err("Failed to create output file")?;
        output_file
            .write_all(file.data())
            .wrap_err("Failed to write data to output file")?;
        Ok(())
    } else {
        Err(eyre!("No file has this hash: {hash}"))
    }
}

fn action_delete(store: &mut SledStorage, hashes: &[String]) -> Result<(), color_eyre::Report> {
    let mut failed = 0;
    let mut succeeded = 0;
    for hash in hashes {
        let result = delete_single_file(store, hash);
        match result {
            Ok(_) => {
                succeeded += 1;
            }
            Err(e) => {
                print!("{}", "FAILED".red());
                println!("{e:?}\n");
                failed += 1;
            }
        }
    }

    println!(
        "Processed {} file{} ({} {} {} {})",
        failed + succeeded,
        if hashes.len() == 1 { "" } else { "s" },
        succeeded,
        "succeeded".green(),
        failed,
        "failed".red(),
    );

    if failed > 0 {
        Err(eyre!("Failed to delete some files"))
    } else {
        Ok(())
    }
}

fn delete_single_file(store: &mut SledStorage, target_hash: &str) -> Result<File> {
    let target = store
        .get_canonical_hash(target_hash)
        .wrap_err("Failed to get canonical hash")?;
    let target = if let Some(target) = target {
        Ok(target)
    } else {
        Err(eyre!("No file has this hash: {target_hash}"))
    }?;
    let res = store
        .delete(&target)
        .wrap_err(format!("While deleting {target}"))?;
    println!("DELETED {}", target.to_string().green());
    Ok(res)
}

fn action_insert(
    store: &mut SledStorage,
    source: &[PathBuf],
    filetype: Option<String>,
) -> Result<()> {
    let mut source = source.to_vec();
    let mut filetype = filetype;

    // If there was no filetype given..
    if filetype.is_none() {
        // and we have at least one file in the list..
        if let Some(last) = source.last().cloned() {
            // and that file doesn't exist..
            if !last.exists() {
                let last_str = last.to_string_lossy();
                // and that file seems to be a filetype..
                if last_str.contains("::") {
                    // TODO: use dialoguer here
                    println!("{last_str} looks a lot like a filetype, but you specified it as a file to be imported!");
                    println!("Make sure to separate the file list from the filetype with '--'.");
                    println!("Example: tbfs insert file1 file2 file3 -- text::plain");

                    println!("Would you like me to interpret {last_str} as a filetype?");
                    print!("[yes]/no: ");
                    std::io::stderr()
                        .flush()
                        .wrap_err("Failed to flush stderr")?;

                    // we ask the user for confirmation
                    let stdin = std::io::stdin();
                    let mut buf = String::new();
                    stdin
                        .read_line(&mut buf)
                        .wrap_err("Failed to read response")?;
                    if 'y'
                        == buf
                            .trim()
                            .to_ascii_lowercase()
                            .chars()
                            .next()
                            .unwrap_or('y')
                    {
                        println!("Interpreting as filetype.");
                        source.pop();
                        filetype = Some(last_str.to_string());
                    } else {
                        println!("Interpreting as filename.");
                    }
                }
            }
        }
    }

    let mut failed = 0;
    let mut succeeded = 0;
    for source in &source {
        let result = insert_single_file(store, source, &filetype);
        match result {
            Ok(_) => {
                succeeded += 1;
            }
            Err(e) => {
                print!("{}", "FAILED".red());
                println!("{e:?}\n");
                failed += 1;
            }
        }
    }

    println!(
        "Processed {} file{} ({} {} {} {})",
        failed + succeeded,
        if source.len() == 1 { "" } else { "s" },
        succeeded,
        "succeeded".green(),
        failed,
        "failed".red(),
    );

    Ok(())
}

fn insert_single_file(
    store: &mut SledStorage,
    source: &PathBuf,
    filetype: &Option<String>,
) -> Result<tbfs::storage::InsertSuccess> {
    print!("Reading {}... ", &source.display());
    std::io::stdout().flush()?;

    if source.is_dir() {
        return Err(eyre!(
            "{} is a directory, can not import directories at this time",
            &source.display()
        ))
        .suggestion("Try importing each file individually");
    }

    let filetype = match filetype {
        Some(filetype) => Filetype::from(filetype.as_str()),
        None => {
            let filetype = match tree_magic_mini::from_filepath(source) {
                Some(filetype) => Filetype::from(filetype),
                None => Filetype::from("data"),
            };
            filetype
        }
    };
    println!("{}", &filetype.to_string().yellow());

    let mut data_file = std::fs::File::open(source).wrap_err("Failed to open source file")?;

    #[allow(clippy::cast_possible_truncation)]
    let mut buf = Vec::with_capacity(
        std::fs::metadata(source)
            .wrap_err("Failed to get file size")?
            .len() as usize,
    );

    let num_bytes = data_file
        .read_to_end(&mut buf)
        .wrap_err("Failed to read source file")?;
    println!(
        "Read {num_bytes} bytes {}",
        if num_bytes > 1000 {
            #[allow(clippy::cast_precision_loss)]
            let num_bytes = num_bytes as f64;
            format!("({})", human_bytes::human_bytes(num_bytes))
        } else {
            String::new()
        }
    );

    let file = File::from_data(&buf, filetype);
    let res = store.insert(&file).wrap_err(format!(
        "Failed to insert file {} into database",
        file.hash()
    ))?;
    match res {
        tbfs::storage::InsertSuccess::ExistingFile => {
            println!("{} {}", "EXISTS".green(), file.hash().to_string().cyan());
        }
        tbfs::storage::InsertSuccess::NewFile => {
            println!("{} {}", "NEW   ".green(), file.hash().to_string().green());
        }
    }
    Ok(res)
}

fn get_datastore_path(args: &Args) -> Result<PathBuf> {
    if let Some(path) = args.datastore.as_ref() {
        Ok(path.clone())
    } else {
        let dirs = get_program_folders().wrap_err("Trying to get default datastore path")?;
        Ok(dirs.data_dir)
    }
}

fn get_tmp_path(tmp_directory: &Option<PathBuf>) -> Result<PathBuf> {
    let dir = if let Some(dir) = tmp_directory {
        dir.clone()
    } else {
        let dirs = get_program_folders().wrap_err("Trying to get cache dir for tmp files")?;
        dirs.cache_dir
    };

    std::fs::create_dir_all(&dir).wrap_err("Failed to create tmp directory")?;
    Ok(dir)
}

fn get_program_folders() -> Result<platform_dirs::AppDirs> {
    // TODO: add clap option to set tmp dir
    platform_dirs::AppDirs::new(Some("tbfs"), false)
        .ok_or(eyre!("Failed to get program folders"))
        .suggestion("Try giving the --datastore option")
}
