# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Support for short names in `tbfs show`
- `tbfs name add` subcommand to add names to files

## [0.8.0] 2023-01-07

### Added
 - `tbfs delete` command to delete files
 - Show number of items listed in `tbfs list`
 - Show total size and size on disk of items listed in `tbfs list`
 - Support for simple tags in `tbfs show`
 - Show number of failed and successful imports
 - Warning when the user gives a filetype argument to `tbfs insert` that will be interpreted as a file to import
 - `tbfs tag add` subcommand to add simple tags to files
 - `tbfs tag get` subcommand to get tags for a file
 - `tbfs tag search` subcommand to get files tagged with regex
 
### Changed
 - Only show warning instead of aborting when the user attempts to import a directory
 - Image printing via the `sixel` protocol is no longer supported

## [0.7.0] 2022-12-25

### Added
 - `--tmp-directory` option to specify location of tmp files
 - `show` subcommand to show a file in the terminal, or open it in an external application
 - Allow the `show` subcommand to embed images in the terminal
 - `show --embed-images` option to control when to embed images in the terminal
 - Show human-readable filesize in `insert`
 - Automatically determine filetype from MIME database
 - Ability to insert multiple files at once
 - Changelog based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
