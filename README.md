![TBFS logo](logo.svg)

# Tag-based filesystem
> Original concept by Nayuki

This is a toy implementation of a Tag-based filesystem, following the design of
[Nayuki](https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies "Original design"). The aim is to provide an store of immutable files with powerful
tag-based search capabilities.

For now, only a barebones cli interface is provided.

## Installing 

Build the project, and then put it somewhere in your `PATH`.

``` bash
cargo +nightly build --release
cp target/release/tbfs ~/.local/bin
```

## Getting started

Start by inserting some files into the datastore:

```bash
echo "Hello TBFS" > some_file.txt
tbfs insert some_file.txt
```

```
Reading some_file.txt...
Determined filetype: text::plain
Read 11 bytes
File hash: 0b7944666fd2b1e7b2a4e470ea6912174ac4086db672b958d3c4db14c7b6033a
Added 1 file
```

As you can see, the filetype was determined automatically.

You can then get the file back out of the store:

```bash
tbfs get 0b some_file_out.txt
cat some_file_out.txt
```

```
Canonical hash: 0b7944666fd2b1e7b2a4e470ea6912174ac4086db672b958d3c4db14c7b6033a
Hello TBFS
```

As you can see, you don't need to specify the full hash of the file, it will
automatically find the correct file for you.

You can also list files:

```bash
tbfs list
```

```
hash                  size   filetype
0b7944666fd2b1e7b2a4  11 B   text::plain
```

As you can see, this also shows the filetype and the size, and truncates the hash to a more reasonable length.

## Testing

Tests expect that `NO_COLOR` is set to `true` to work correctly.

## Licensing

The code in this project is licensed under [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) or later.

The logo of this project is licensed under [Creative Commons Attribution-NonCommercial-ShareAlike](http://creativecommons.org/licenses/by-nc-sa/4.0/)
