use std::io::Write;

use assert_cmd::Command;
use tbfs::test_utils::{test_file, test_path};

#[test]
fn show_works() -> Result<(), Box<dyn std::error::Error>> {
    let (mut file1, path1) = test_file("show_works", "file1");
    file1.write_all(b"this is file1")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("show_works");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(path1);
    cmd.assert().success();

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("show").arg("adc1f6e0aaa71ef83e60");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}

#[test]
fn show_tag() -> Result<(), Box<dyn std::error::Error>> {
    let (mut file1, path1) = test_file("show_tag", "file1");
    file1.write_all(b"this is file1")?;

    let test_path = test_path("show_tag");

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(path1);
    cmd.assert().success();

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("tag")
        .arg("add")
        .arg("adc1f6e0aaa71ef83e60")
        .arg("--")
        .arg("Testing tag content");
    cmd.assert().success();

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("show").arg("4f9457c1d0a2f7bb43");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}
