use std::io::Write;

use assert_cmd::Command;
use color_eyre::Result;

use tbfs::test_utils::{test_file, test_path};

mod search {
    use tbfs::{
        file::{File, Filetype},
        filetypes::simple_tag::SimpleTag,
        prelude::*,
        storage::{SledStorage, Storage},
    };

    #[allow(clippy::wildcard_imports)]
    use super::*;

    #[test]
    fn search_works() -> Result<()> {
        let path = test_path("search_works");
        let mut store = SledStorage::new(&path)?;

        let file1 = File::from_data(b"this is file1", Filetype::from("text::plain"));
        let file2 = File::from_data(
            b"this is file2, slightly bigger",
            Filetype::from("some::other::filetype"),
        );
        let file3 = File::from_data(
            b"this is file3, the biggest of them all",
            Filetype::from("text::plain"),
        );

        store.insert(&file1)?;
        store.insert(&file2)?;
        store.insert(&file3)?;

        add_tag(&mut store, file1.hash(), "author: nico_nico")?;
        add_tag(&mut store, file1.hash(), "author: logistic-bot")?;
        add_tag(&mut store, file1.hash(), "owner: nico_nico")?;

        add_tag(&mut store, file2.hash(), "author: nico_nico")?;
        add_tag(&mut store, file2.hash(), "owner: logistic-bot")?;
        add_tag(&mut store, file2.hash(), "medium file")?;

        add_tag(&mut store, file3.hash(), "owner: nico_nico")?;
        add_tag(&mut store, file3.hash(), "author: logistic-bot")?;
        add_tag(&mut store, file3.hash(), "large file")?;

        drop(store);

        // File hashes:
        // 1. adc1f6e0aaa71ef83e60bff6c5e8e0606c127fb3234792a7ba416d88c4017517
        // 2. 7d5a6c6098220881d18ad7c76fbc9106b454409ecf03a2bf721074427c167daa
        // 3. c3c22da74ea5e6f1228245e1a3c4eab01c74af76489f66f59180ba97f20cf834

        let single_file_perfect_match = get_results(&path, "medium file")?;
        insta::assert_snapshot!(single_file_perfect_match);

        let all_files_starting_regex = get_results(&path, "owner:.*")?;
        insta::assert_snapshot!(all_files_starting_regex);

        let all_files_ending_regex = get_results(&path, ".*logistic-bot")?;
        insta::assert_snapshot!(all_files_ending_regex);

        let one_and_three_by_author = get_results(&path, "author: logistic-bot")?;
        insta::assert_snapshot!(one_and_three_by_author);

        let one_and_two_by_author = get_results(&path, "author: nico_nico")?;
        insta::assert_snapshot!(one_and_two_by_author);

        let all_files_with_glob = get_results(&path, ".*")?;
        insta::assert_snapshot!(all_files_with_glob);

        let no_files_with_empty = get_results(&path, "")?;
        insta::assert_snapshot!(no_files_with_empty);

        Ok(())
    }

    fn get_results(path: &std::path::PathBuf, search: &str) -> Result<String> {
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(path);
        cmd.arg("tag").arg("search").arg(search);
        let output = cmd.output()?;
        let raw_stdout = output.stdout;
        let output = String::from_utf8_lossy(&raw_stdout);
        Ok(output.to_string())
    }

    fn add_tag(
        store: &mut SledStorage,
        file: &Hash,
        content: &str,
    ) -> Result<(), color_eyre::Report> {
        let tag = SimpleTag::new(file.clone(), String::from(content));
        let tag_file = File::try_from(tag)?;
        store.insert(&tag_file)?;
        Ok(())
    }
}

mod add {
    #[allow(clippy::wildcard_imports)]
    use super::*;

    #[test]
    fn tag_single_file() -> Result<(), Box<dyn std::error::Error>> {
        // insert some files
        let (mut file1, path1) = test_file("tag_single_file", "file1");
        file1.write_all(b"this is file1")?;
        let (mut file2, path2) = test_file("tag_single_file", "file2");
        file2.write_all(b"this is file2, slightly bigger")?;
        let (mut file3, path3) = test_file("tag_single_file", "file3");
        file3.write_all(b"this is file3, the biggest of them all")?;

        let mut cmd = Command::cargo_bin("tbfs")?;
        let test_path = test_path("tag_single_file");
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert").arg(path1).arg(path3);
        cmd.assert().success();

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert")
            .arg(path2)
            .arg("--")
            .arg("some::other::filetype");
        cmd.assert().success();

        // assert that only 3 files were inserted
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // tag one of the files
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is some content for a tag used for testing");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // assert that a tag file was created
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        Ok(())
    }

    #[test]
    fn tag_multiple_files() -> Result<(), Box<dyn std::error::Error>> {
        // insert some files
        let (mut file1, path1) = test_file("tag_multiple_files", "file1");
        file1.write_all(b"this is file1")?;
        let (mut file2, path2) = test_file("tag_multiple_files", "file2");
        file2.write_all(b"this is file2, slightly bigger")?;
        let (mut file3, path3) = test_file("tag_multiple_files", "file3");
        file3.write_all(b"this is file3, the biggest of them all")?;

        let mut cmd = Command::cargo_bin("tbfs")?;
        let test_path = test_path("tag_multiple_files");
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert").arg(path1).arg(path3);
        cmd.assert().success();

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert")
            .arg(path2)
            .arg("--")
            .arg("some::other::filetype");
        cmd.assert().success();

        // assert that only 3 files were inserted
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // tag two of the files
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("7d5a6c6098220881d18a")
            .arg("--")
            .arg("This tag is applied to two files, so it should create two tag files");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // assert that a tag file was created for each tagged file
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        Ok(())
    }

    #[test]
    fn tag_multiline() -> Result<(), Box<dyn std::error::Error>> {
        // insert some files
        let (mut file1, path1) = test_file("tag_multiline", "file1");
        file1.write_all(b"this is file1")?;
        let (mut file2, path2) = test_file("tag_multiline", "file2");
        file2.write_all(b"this is file2, slightly bigger")?;
        let (mut file3, path3) = test_file("tag_multiline", "file3");
        file3.write_all(b"this is file3, the biggest of them all")?;

        let mut cmd = Command::cargo_bin("tbfs")?;
        let test_path = test_path("tag_multiline");
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert").arg(path1).arg(path3);
        cmd.assert().success();

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert")
            .arg(path2)
            .arg("--")
            .arg("some::other::filetype");
        cmd.assert().success();

        // assert that only 3 files were inserted
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // tag two of the files
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("7d5a6c6098220881d18a")
            .arg("--")
            .arg(
                "This tag has multiple lines\nThis is the second line\nAnd here is the third line",
            );
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // assert that a tag file was created for each tagged file
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        Ok(())
    }

    #[test]
    fn tag_file_twice_changes_nothing() -> Result<(), Box<dyn std::error::Error>> {
        // insert some files
        let (mut file1, path1) = test_file("tag_file_twice_changes_nothing", "file1");
        file1.write_all(b"this is file1")?;
        let (mut file2, path2) = test_file("tag_file_twice_changes_nothing", "file2");
        file2.write_all(b"this is file2, slightly bigger")?;
        let (mut file3, path3) = test_file("tag_file_twice_changes_nothing", "file3");
        file3.write_all(b"this is file3, the biggest of them all")?;

        let mut cmd = Command::cargo_bin("tbfs")?;
        let test_path = test_path("tag_file_twice_changes_nothing");
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert").arg(path1).arg(path3);
        cmd.assert().success();

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert")
            .arg(path2)
            .arg("--")
            .arg("some::other::filetype");
        cmd.assert().success();

        // assert that only 3 files were inserted
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // tag one of the files
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is some content for a tag used for testing");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // assert that a tag file was created
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // tag one of the files again
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is some content for a tag used for testing");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        // assert that no new file was created
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        Ok(())
    }
}

mod get {
    #[allow(clippy::wildcard_imports)]
    use super::*;

    #[test]
    fn get_single_tag() -> Result<(), Box<dyn std::error::Error>> {
        // insert some files
        let (mut file1, path1) = test_file("get_single_tag", "file1");
        file1.write_all(b"this is file1")?;
        let (mut file2, path2) = test_file("get_single_tag", "file2");
        file2.write_all(b"this is file2, slightly bigger")?;
        let (mut file3, path3) = test_file("get_single_tag", "file3");
        file3.write_all(b"this is file3, the biggest of them all")?;

        let mut cmd = Command::cargo_bin("tbfs")?;
        let test_path = test_path("get_single_tag");
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert").arg(path1).arg(path3);
        cmd.assert().success();

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert")
            .arg(path2)
            .arg("--")
            .arg("some::other::filetype");
        cmd.assert().success();

        // assert that only 3 files were inserted
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        cmd.assert().success();

        // tag one of the files
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is some content for a tag used for testing");
        cmd.assert().success();

        // get tags for that file
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag").arg("get").arg("adc1f6e0aaa71ef83e60");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        Ok(())
    }

    #[test]
    fn get_multiple_tags() -> Result<(), Box<dyn std::error::Error>> {
        // insert some files
        let (mut file1, path1) = test_file("get_multiple_tags", "file1");
        file1.write_all(b"this is file1")?;
        let (mut file2, path2) = test_file("get_multiple_tags", "file2");
        file2.write_all(b"this is file2, slightly bigger")?;
        let (mut file3, path3) = test_file("get_multiple_tags", "file3");
        file3.write_all(b"this is file3, the biggest of them all")?;

        let mut cmd = Command::cargo_bin("tbfs")?;
        let test_path = test_path("get_multiple_tags");
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert").arg(path1).arg(path3);
        cmd.assert().success();

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert")
            .arg(path2)
            .arg("--")
            .arg("some::other::filetype");
        cmd.assert().success();

        // assert that only 3 files were inserted
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        cmd.assert().success();

        // tag one file twice
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is tag #1");
        cmd.assert().success();
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is tag #2");
        cmd.assert().success();

        // assert that both tags show up
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag").arg("get").arg("adc1f6e0aaa71ef83e60");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        Ok(())
    }

    #[test]
    fn get_multiline_tags() -> Result<(), Box<dyn std::error::Error>> {
        // insert some files
        let (mut file1, path1) = test_file("get_multiline_tags", "file1");
        file1.write_all(b"this is file1")?;
        let (mut file2, path2) = test_file("get_multiline_tags", "file2");
        file2.write_all(b"this is file2, slightly bigger")?;
        let (mut file3, path3) = test_file("get_multiline_tags", "file3");
        file3.write_all(b"this is file3, the biggest of them all")?;

        let mut cmd = Command::cargo_bin("tbfs")?;
        let test_path = test_path("get_multiline_tags");
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert").arg(path1).arg(path3);
        cmd.assert().success();

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("insert")
            .arg(path2)
            .arg("--")
            .arg("some::other::filetype");
        cmd.assert().success();

        // assert that only 3 files were inserted
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("list");
        cmd.assert().success();

        // tag one file three times
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is tag #1\nLine two of tag #1\nThis is still tag #1");
        cmd.assert().success();
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("This is tag #2\nTag #2 has a second line\nAnd a third too\nThis is the last line of tag #2");
        cmd.assert().success();
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag")
            .arg("add")
            .arg("adc1f6e0aaa71ef83e60")
            .arg("--")
            .arg("Tag #3 only has one line");
        cmd.assert().success();

        // assert that all tags show up, and are distinguishable from each other
        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&test_path);
        cmd.arg("tag").arg("get").arg("adc1f6e0aaa71ef83e60");
        let output = cmd.output()?;
        insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

        Ok(())
    }
}
