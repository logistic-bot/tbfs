use std::io::Write;

use assert_cmd::Command;
use tbfs::test_utils::{test_file, test_path};

#[test]
fn delete_single_file() -> Result<(), Box<dyn std::error::Error>> {
    // insert some files
    let (mut file1, path1) = test_file("delete_single_file", "file1");
    file1.write_all(b"this is file1")?;
    let (mut file2, path2) = test_file("delete_single_file", "file2");
    file2.write_all(b"this is file2, slightly bigger")?;
    let (mut file3, path3) = test_file("delete_single_file", "file3");
    file3.write_all(b"this is file3, the biggest of them all")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("delete_single_file");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(path1).arg(path3);
    cmd.assert().success();

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert")
        .arg(path2)
        .arg("--")
        .arg("some::other::filetype");
    cmd.assert().success();

    // assert that only 3 files were inserted
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    // delete one of the files
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("delete").arg("adc1f6e0aaa71ef83e60");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    // assert that a file was deleted
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}

#[test]
fn delete_multiple_file() -> Result<(), Box<dyn std::error::Error>> {
    // insert some files
    let (mut file1, path1) = test_file("delete_multiple_file", "file1");
    file1.write_all(b"this is file1")?;
    let (mut file2, path2) = test_file("delete_multiple_file", "file2");
    file2.write_all(b"this is file2, slightly bigger")?;
    let (mut file3, path3) = test_file("delete_multiple_file", "file3");
    file3.write_all(b"this is file3, the biggest of them all")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("delete_multiple_file");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(path1).arg(path3);
    cmd.assert().success();

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert")
        .arg(path2)
        .arg("--")
        .arg("some::other::filetype");
    cmd.assert().success();

    // assert that only 3 files were inserted
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    // delete two of the files
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("delete")
        .arg("adc1f6e0aaa71ef83e60")
        .arg("7d5a6c6098220881d18a");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    // assert that two files was deleted
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}
