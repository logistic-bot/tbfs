use assert_cmd::prelude::*;
use std::{io::Write, process::Command};

#[cfg(test)]
use tbfs::test_utils::{test_file, test_path};

#[test]
fn insert_nonexistent_file_fails() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("tbfs")?;

    cmd.arg("insert").arg("test/file/doesnt/exist");
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));
    Ok(())
}

#[test]
fn insert_works() -> Result<(), Box<dyn std::error::Error>> {
    let (mut file1, path1) = test_file("insert_works", "file1");
    file1.write_all(b"this is file1")?;
    let (mut file2, path2) = test_file("insert_works", "file2");
    file2.write_all(b"this is file2")?;
    let (mut file3, path3) = test_file("insert_works", "file3");
    file3.write_all(b"this is file3")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("insert_works");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(path1).arg(path2).arg(path3);

    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}

#[test]
fn insert_same_file_does_not_duplicate() -> Result<(), Box<dyn std::error::Error>> {
    let (mut file1, path1) = test_file("insert_same_file_does_not_duplicate", "path1");
    file1.write_all(b"this is file1")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("insert_same_file_does_not_duplicate");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(&path1);
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(&path1);
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}

#[test]
fn insert_same_file_changes_filetype() -> Result<(), Box<dyn std::error::Error>> {
    let (mut file1, path1) = test_file("insert_same_file_changes_filetype", "path1");
    file1.write_all(b"this is file1")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("insert_same_file_changes_filetype");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(&path1).arg("--").arg("text::plain");
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert")
        .arg(&path1)
        .arg("--")
        .arg("test::filetype");
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output().unwrap();
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}

#[test]
fn insert_directory_shows_warning() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("insert_directory_shows_warning");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert")
        .arg(&test_path)
        .arg("--")
        .arg("text::plain");
    let output = cmd.output().unwrap();

    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));

    Ok(())
}
