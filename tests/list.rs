use std::io::Write;

use assert_cmd::Command;
use tbfs::test_utils::{test_file, test_path};

#[test]
fn list_works() -> Result<(), Box<dyn std::error::Error>> {
    let (mut file1, path1) = test_file("list_works", "file1");
    file1.write_all(b"this is file1")?;
    let (mut file2, path2) = test_file("list_works", "file2");
    file2.write_all(b"this is file2, slightly bigger")?;
    let (mut file3, path3) = test_file("list_works", "file3");
    file3.write_all(b"this is file3, the biggest of them all")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("list_works");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(path1).arg(path3);

    cmd.assert().success();

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert")
        .arg(path2)
        .arg("--")
        .arg("some::other::filetype");

    cmd.assert().success();

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list").arg("--filetype").arg("false");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list").arg("--human").arg("false");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list").arg("--full-hash");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list").arg("--sort").arg("size");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list").arg("--sort").arg("hash");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("list").arg("--sort").arg("filetype");
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));

    Ok(())
}
