use assert_cmd::Command;
use color_eyre::Result;
use function_name::named;
use tbfs::test_utils::{add_default_files, add_default_files_with_names, test_path};

// File hashes:
// 1. adc1f6e0aaa71ef83e60bff6c5e8e0606c127fb3234792a7ba416d88c4017517
// 2. 7d5a6c6098220881d18ad7c76fbc9106b454409ecf03a2bf721074427c167daa
// 3. c3c22da74ea5e6f1228245e1a3c4eab01c74af76489f66f59180ba97f20cf834

#[test]
#[named]
fn name_get_empty_datastore() -> Result<()> {
    let path = test_path(function_name!());

    let path = &path;
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(path);
    cmd.arg("name").arg("get");
    let output = cmd.output()?;
    assert!(output.status.success());
    let output = String::from_utf8_lossy(&output.stdout).to_string();
    insta::assert_snapshot!(output);

    Ok(())
}

#[test]
#[named]
fn name_get_all_files() -> Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files_with_names(&path)?;
    drop(store);

    let path = &path;
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(path);
    cmd.arg("name").arg("get");
    let output = cmd.output()?;
    assert!(output.status.success());
    let output = String::from_utf8_lossy(&output.stdout).to_string();
    insta::assert_snapshot!(output);

    Ok(())
}

#[test]
#[named]
fn name_get_multiple_files_with_name() -> Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files_with_names(&path)?;
    drop(store);

    let path = &path;
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(path);
    cmd.arg("name")
        .arg("get")
        .arg("adc1f6e0aaa71ef83e60bff6c5e8e0606c127fb3234792a7ba416d88c4017517")
        .arg("7d5a6c6098220881d18ad7c76fbc9106b454409ecf03a2bf721074427c167daa")
        .arg("c3c22da74ea5e6f1228245e1a3c4eab01c74af76489f66f59180ba97f20cf834");
    let output = cmd.output()?;
    assert!(output.status.success());
    let output = String::from_utf8_lossy(&output.stdout).to_string();
    insta::assert_snapshot!(output);

    Ok(())
}

#[test]
#[named]
fn name_get_multiple_files_without_name() -> Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files(&path)?;
    drop(store);

    let path = &path;
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(path);
    cmd.arg("name")
        .arg("get")
        .arg("adc1f6e0aaa71ef83e60bff6c5e8e0606c127fb3234792a7ba416d88c4017517")
        .arg("7d5a6c6098220881d18ad7c76fbc9106b454409ecf03a2bf721074427c167daa")
        .arg("c3c22da74ea5e6f1228245e1a3c4eab01c74af76489f66f59180ba97f20cf834");
    let output = cmd.output()?;
    assert!(output.status.success());
    let output = String::from_utf8_lossy(&output.stdout).to_string();
    insta::assert_snapshot!(output);

    Ok(())
}

#[test]
#[named]
fn name_get_single_file_without_name() -> Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files(&path)?;
    drop(store);

    let hash = "c3c22da74ea5e6f1228245e1a3c4eab01c74af76489f66f59180ba97f20cf834";
    let output = name_get_result(&path, hash)?;
    insta::assert_snapshot!(output);

    Ok(())
}

#[test]
#[named]
fn name_get_single_file_with_name() -> Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files_with_names(&path)?;
    drop(store);

    let hash = "adc1f6e0aaa71ef83e60bff6c5e8e0606c127fb3234792a7ba416d88c4017517";
    let output = name_get_result(&path, hash)?;
    insta::assert_snapshot!(output);

    let hash = "7d5a6c6098220881d18ad7c76fbc9106b454409ecf03a2bf721074427c167daa";
    let output = name_get_result(&path, hash)?;
    insta::assert_snapshot!(output);

    let hash = "c3c22da74ea5e6f1228245e1a3c4eab01c74af76489f66f59180ba97f20cf834";
    let output = name_get_result(&path, hash)?;
    insta::assert_snapshot!(output);

    Ok(())
}

fn name_get_result(path: &std::path::PathBuf, hash: &str) -> Result<String, color_eyre::Report> {
    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(path);
    cmd.arg("name").arg("get").arg(hash);
    let output = cmd.output()?;
    assert!(output.status.success());
    let output = String::from_utf8_lossy(&output.stdout).to_string();
    Ok(output)
}

#[test]
#[named]
fn name_shows_up_in_list() -> Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files_with_names(&path)?;
    drop(store);

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&path);
    cmd.arg("list").arg("-N");
    let output = cmd.output()?;
    assert!(output.status.success());
    let output = String::from_utf8_lossy(&output.stdout).to_string();
    insta::assert_snapshot!(output);

    Ok(())
}

#[test]
#[named]
fn name_shows_up_in_show() -> Result<()> {
    let path = test_path(function_name!());
    let store = add_default_files(&path)?;

    drop(store);

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&path);
    cmd.arg("name").arg("add").arg("7d5a").arg("File #2");
    let output = cmd.output()?;
    assert!(output.status.success());

    // name file hash: 2ef11bb5c0332e60802721dd8f1effde74c5815999250549944fa145e55ae663

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&path);
    cmd.arg("show").arg("2ef1");
    let output = cmd.output()?;
    assert!(output.status.success());
    let output = String::from_utf8_lossy(&output.stdout).to_string();
    insta::assert_snapshot!(output);

    Ok(())
}

mod add {
    #[allow(clippy::wildcard_imports)]
    use super::*;

    #[test]
    #[named]
    fn add_works() -> Result<()> {
        let path = test_path(function_name!());
        let store = add_default_files(&path)?;

        drop(store);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(path);
        cmd.arg("name").arg("add").arg("7d5a").arg("File #2");
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        Ok(())
    }

    #[test]
    #[named]
    fn add_same_name_multiple_times() -> Result<()> {
        let path = test_path(function_name!());
        let store = add_default_files(&path)?;

        drop(store);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&path);
        cmd.arg("name").arg("add").arg("7d5a").arg("File #2");
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&path);
        cmd.arg("name").arg("add").arg("7d5a").arg("File #2");
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        Ok(())
    }

    #[test]
    #[named]
    fn add_multiple_names() -> Result<()> {
        let path = test_path(function_name!());
        let store = add_default_files(&path)?;

        drop(store);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&path);
        cmd.arg("name").arg("add").arg("7d5a").arg("File #2");
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&path);
        cmd.arg("name")
            .arg("add")
            .arg("7d5a")
            .arg("Another name for File #2");
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&path);
        cmd.arg("name")
            .arg("add")
            .arg("7d5a")
            .arg("A third name for File #2");
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        Ok(())
    }

    #[test]
    #[named]
    fn add_long_name() -> Result<()> {
        let path = test_path(function_name!());
        let store = add_default_files(&path)?;

        drop(store);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(&path);
        cmd.arg("name").arg("add").arg("7d5a").arg(
            "This is file #2. It is one of the default files that are generated in tests. This is a filename for it. It is very long. This is to test that there are no problems when adding very long filenames. Filenames such as these. Alright, this filename is more than 200 characters now I think, so that's probably enough."
        );
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        Ok(())
    }

    #[test]
    #[named]
    fn add_multiline_name() -> Result<()> {
        let path = test_path(function_name!());
        let store = add_default_files(&path)?;

        drop(store);

        let mut cmd = Command::cargo_bin("tbfs")?;
        cmd.arg("--datastore").arg(path);
        cmd.arg("name").arg("add").arg("7d5a").arg(
            "This is file #2.\nIt is one of the default files that are generated in tests.\nThis is a filename for it.\nIt is composed of sevral lines.\nThis is to test that there are no problems when adding very long and multiline filenames.\nFilenames such as these.\nAlright, this filename is more than 200 characters now I think, so that's probably enough."
        );
        let output = cmd.output()?;
        assert!(output.status.success());
        let output = String::from_utf8_lossy(&output.stdout).to_string();
        insta::assert_snapshot!(output);

        Ok(())
    }
}
