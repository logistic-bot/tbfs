use std::io::Write;

use assert_cmd::Command;
use assert_fs::assert::PathAssert;
use tbfs::test_utils::{test_file, test_path};

#[test]
fn get_works() -> Result<(), Box<dyn std::error::Error>> {
    let (mut file1, path1) = test_file("get_works", "file1");
    file1.write_all(b"this is file1")?;
    let (mut file2, path2) = test_file("get_works", "file2");
    file2.write_all(b"this is file2")?;
    let (mut file3, path3) = test_file("get_works", "file3");
    file3.write_all(b"this is file3")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    let test_path = test_path("get_works");
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("insert").arg(path1).arg(path2).arg(path3);

    cmd.assert().success();

    let output_file = assert_fs::NamedTempFile::new("file1_out")?;

    let mut cmd = Command::cargo_bin("tbfs")?;
    cmd.arg("--datastore").arg(&test_path);
    cmd.arg("get")
        .arg("adc1f6e0aaa71ef83e60")
        .arg(output_file.path());
    let output = cmd.output()?;
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stderr));
    insta::assert_display_snapshot!(String::from_utf8_lossy(&output.stdout));
    output_file.assert(predicates::str::diff("this is file1"));
    Ok(())
}
