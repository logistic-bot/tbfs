# Color signification guidelines

These are not hard rules.

- white
  Generic file content, or non-tbfs filepath.
- grey
  File hash.
- blue
  File we are currently working on.
- cyan
  File already existed, but would have been created.
- green
  File was newly created, or operation success.
- red
  Operation failure.
- yellow
  Filetype, or reference to other file, or warning.
